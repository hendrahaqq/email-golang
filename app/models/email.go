package models

import (
	"time"

	"gorm.io/gorm"
)

//Email comment
type Email struct {
	gorm.Model
	To      string
	Subject string
	Message string
	SendAt  time.Time
	IsSent  bool
}
