package controller

import (
	"assesment-6-2/app/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateTodos comment.
func (strDB *StrDB) CreateTodos(c *gin.Context) {
	var (
		todo   models.Todos
		result gin.H
	)

	err := c.Bind(&todo)
	if err != nil {
		fmt.Println("tidak ada data")
	}
	strDB.DB.Create(&todo)
	result = gin.H{
		"result": todo,
	}
	c.JSON(http.StatusOK, result)
}

// DeleteTodos comment.
func (strDB *StrDB) DeleteTodos(c *gin.Context) {
	var (
		todo   models.Todos
		result gin.H
	)
	id := c.Param("id")
	err := strDB.DB.First(&todo, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	fmt.Println(todo)
	err = strDB.DB.Delete(&todo).Error
	if err != nil {
		result = gin.H{
			"result": "delete failed",
		}
	} else {
		result = gin.H{
			"result": "Data deleted successfully",
		}

	}

	c.JSON(http.StatusOK, result)
}

// UpdateTodos comment.
func (strDB *StrDB) UpdateTodos(c *gin.Context) {
	var (
		todo     models.Todos
		newTodos models.Todos
		result   gin.H
	)

	id := c.Param("id")
	err := c.Bind(&newTodos)

	err = strDB.DB.First(&todo, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}

	err = strDB.DB.Model(&todo).Updates(newTodos).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "update succesful",
		}
	}
	c.JSON(http.StatusOK, result)
}

// GetTodos comment.
func (strDB *StrDB) GetTodos(c *gin.Context) {
	var (
		todo   []models.Todos
		result gin.H
	)

	strDB.DB.Find(&todo)
	if len(todo) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		result = responseAPI(todo, len(todo))
	}
	c.JSON(http.StatusOK, result)
}

// GetOneTodos comment.
func (strDB *StrDB) GetOneTodos(c *gin.Context) {
	var (
		todo   []models.Todos
		result gin.H
	)

	id := c.Param("id")
	strDB.DB.First(&todo, id)
	if len(todo) <= 0 {
		result = gin.H{
			"result": nil,
		}
	} else {
		result = gin.H{
			"result": todo,
		}
	}

	c.JSON(http.StatusOK, result)
}

// GetSearchTodos comment.
func (strDB *StrDB) GetSearchTodos(c *gin.Context) {
	var (
		todo   []models.Todos
		result gin.H
	)

	name := c.DefaultQuery("name", "")
	desc := c.DefaultQuery("description", "")
	user := c.DefaultQuery("user_id", "")
	strDB.DB.Where("name LIKE ? OR description LIKE ? OR user_id LIKE ?", name, desc, user).Find(&todo)
	if len(todo) <= 0 {
		result = gin.H{
			"result": nil,
		}
	} else {
		result = responseAPI(todo, len(todo))
	}

	c.JSON(http.StatusOK, result)
}

//TodosUser Comment.
func (strDB *StrDB) TodosUser(c *gin.Context) {
	var (
		todo   []models.Todos
		result gin.H
	)

	strDB.DB.Joins("User").Find(&todo)
	if len(todo) <= 0 {
		result = gin.H{
			"result":  nil,
			"message": "Data Not Found",
		}
	} else {
		result = gin.H{
			"result": responseAPI(todo, len(todo)),
		}
	}

	c.JSON(http.StatusOK, result)
}

//GetTodosUser comment.
func (strDB *StrDB) GetTodosUser(c *gin.Context) {
	var (
		todo   []models.Todos
		result gin.H
	)

	id := c.Param("id")
	strDB.DB.Joins("User").First(&todo, id)
	if len(todo) <= 0 {
		result = gin.H{
			"result":  nil,
			"message": "Data Not Found",
		}
	} else {
		result = gin.H{
			"result": todo,
		}
	}

	c.JSON(http.StatusOK, result)
}
