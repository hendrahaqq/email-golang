package controller

import (
	"assesment-6-2/app/models"
	"fmt"
	"net/smtp"
	"os"
	"strings"
	"time"
)

var (
	sendTime = time.Now().Unix()
)

//AddEmail comment.
func (strDB *StrDB) AddEmail() {
	timesent, _ := time.Parse(time.RFC3339, "2020-10-14T16:52:00+07:00")
	email := models.Email{
		To:      "cahyo.add.qt@gmail.com",
		Subject: "test email",
		Message: "halo mas cahyo",
		SendAt:  timesent,
		IsSent:  false,
	}

	strDB.DB.Create(&email)
	fmt.Println("data berhasil")
}

//SendEmail comment.
func (strDB *StrDB) SendEmail() {
	fmt.Println("cek ada data ga ya?")
	var (
		email []models.Email
	)

	strDB.DB.Where("is_sent = ?", false).Find(&email)

	for _, value := range email {
		var to []string
		to = append(to, value.To)
		if value.SendAt.Unix() <= sendTime {
			fmt.Println("ada data nih mau dikirim")
			err := SendMail(to, value.Subject, value.Message)
			if err != nil {
				fmt.Println("failed")
			} else {
				fmt.Println("success")
				value.IsSent = true
				strDB.DB.Save(&value)
			}
		}
	}
}

//SendMail comment.
func SendMail(to []string, subject, message string) error {
	body := "From: " + os.Getenv("CONFIG_EMAIL") + "\n" + "To: " + strings.Join(to, ",") + "\n" + "Subject: " + subject + "\n\n" + message

	auth := smtp.PlainAuth("", os.Getenv("CONFIG_EMAIL"), os.Getenv("CONFIG_PASSWORD"), os.Getenv("CONFIG_SMTP_HOST"))

	smtpAddr := os.Getenv("CONFIG_SMTP_HOST") + ":" + os.Getenv("CONFIG_SMTP_PORT")
	err := smtp.SendMail(smtpAddr, auth, os.Getenv("CONFIG_EMAIL"), append(to), []byte(body))

	if err != nil {
		return err
	}

	return nil
}
