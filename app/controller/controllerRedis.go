package controller

import (
	"assesment-6-2/app/models"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/gomodule/redigo/redis"
)

// GetOfficeWithoutRedis comment.
func (strDB *StrDB) GetOfficeWithoutRedis(c *gin.Context) {
	var (
		office []models.Office
		count  int64
		result gin.H
	)

	//path
	endpoint := "without-redis/"
	pathStaging := os.Getenv("URL_STAGING")

	//current page
	current := c.DefaultQuery("current", "1")
	currentPage, _ := strconv.Atoi(current)

	//limit perpage
	limit, _ := strconv.Atoi(c.DefaultQuery("per_page", "5"))
	perPage := float32(limit)

	//count all
	strDB.DB.Table("offices").Count(&count)

	//select with limit
	strDB.DB.Limit(limit).Find(&office)

	if len(office) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		// result = responseAPI(office, len(office))
		result = responsePagination(office, perPage, count, float32(currentPage), pathStaging, endpoint)
	}
	c.JSON(http.StatusOK, result)
}

// GetOfficeWithRedis comment.
func (strDB *StrDB) GetOfficeWithRedis(c *gin.Context) {
	var (
		office []models.Office
		count  int64
		result gin.H
	)

	pool := redis.NewPool(
		func() (redis.Conn, error) {
			return redis.Dial("tcp", "127.0.0.1:6379")
		},
		10,
	)

	pool.MaxActive = 0

	//mengambil satu koneksi dari pool
	conn := pool.Get()
	defer conn.Close()

	//path
	endpoint := "with-redis/"
	pathStaging := os.Getenv("URL_STAGING")

	//current page
	current := c.DefaultQuery("page", "1")
	currentPage, _ := strconv.Atoi(current)

	//limit perpage
	limit, _ := strconv.Atoi(c.DefaultQuery("per_page", "5"))
	perPage := float32(limit)

	reply, err := redis.Bytes(conn.Do("GET", "office:1"))
	if err != nil {
		jd, _ := json.Marshal(result)

		//count all
		strDB.DB.Table("offices").Count(&count)

		//select with limit
		strDB.DB.Limit(limit).Find(&office)
		_, _ = conn.Do("SET", "office:1", string(jd))

	}

	if len(office) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		// result = responseAPI(office, len(office))
		result = responsePagination(office, perPage, count, float32(currentPage), pathStaging, endpoint)
	}

	var sec map[string]interface{}
	err = json.Unmarshal(reply, &sec)
	if err != nil {
		fmt.Println(err)
	}

	c.JSON(http.StatusOK, sec)
}
